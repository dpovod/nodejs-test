const {describe, it} = require('mocha');
const expect = require('chai').expect;
import SequenceService from './../app/services/SequenceService';
import NumericSequence from './../app/valueObjects/NumericSequence';

describe("SequenceService::getLongestSequence: test sequences one number long", function () {
    it("Sequence with only one '1' should return empty sequence", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1]), 0);
        expect(longestSequence.members).deep.equal([]);
    });

    it("Sequence with only one '0' should return this sequence", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0]), 0);
        expect(longestSequence.members).deep.equal([0]);
    });
});

describe("SequenceService::getLongestSequence: test empty sequence", function () {
    it("Empty sequence should return empty sequence", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([]), 0);
        expect(longestSequence.members).deep.equal([]);
    });
});

describe("SequenceService::getLongestSequence: test sequence with more consecutive '1's than '0's", function () {
    it("Sequence with 4 consecutive '1's and 3 consecutive '0's should return sequence with 3 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1, 1, 1, 1, 0, 0, 0]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0]);
    });

    it("Sequence with 3 consecutive '0's and 4 consecutive '1's should return sequence with 3 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0, 0, 0, 1, 1, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0]);
    });

    it("This sequence should return sequence with 3 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0, 0, 0, 1, 1, 1, 1, 0, 0]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0]);
    });

    it("This sequence should return sequence with 3 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0]);
    });

    it("This sequence should return sequence with 2 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1, 1, 0, 0, 1, 1, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0]);
    });

    it("This sequence should return sequence with 2 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0]);
    });
});

describe("SequenceService::getLongestSequence: test sequence with more consecutive '0's than '1's", function () {
    it("Sequence with 3 consecutive '1's and 4 consecutive '0's should return sequence with 4 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([1, 1, 1, 0, 0, 0, 0]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0, 0]);
    });

    it("Sequence with 5 consecutive '0's and 4 consecutive '1's should return sequence with 5 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0, 0, 0, 0, 0, 1, 1, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0, 0, 0]);
    });

    it("This sequence should return should return sequence with 5 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0, 0, 0]);
    });

    it("This sequence should return should return sequence with 6 consecutive '0's", function () {
        const longestSequence = SequenceService.getLongestSequence(new NumericSequence([0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1]), 0);
        expect(longestSequence.members).deep.equal([0, 0, 0, 0, 0, 0]);
    });
});

describe("SequenceService::generateOrderedSequence: basic test", function () {
    it("Should return new sequence with ascending order by sequences length and alternating 1 sequences and 0 sequences", function () {
        const orderedSequence = SequenceService.generateOrderedSequence(new NumericSequence([1, 1, 0, 1, 0, 0, 0]), [0, 1]);
        expect(orderedSequence.members).deep.equal([1, 0, 1, 1, 0, 0, 0]);
    });

    it("Should return new sequence with ascending order by sequences length and alternating 1 sequences and 0 sequences", function () {
        const orderedSequence = SequenceService.generateOrderedSequence(new NumericSequence([1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0]), [0, 1]);
        expect(orderedSequence.members).deep.equal([1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0]);
    });
});
