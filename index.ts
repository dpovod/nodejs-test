import NumericSequence from "./app/valueObjects/NumericSequence";
import SequenceService from './app/services/SequenceService';
import InvalidSequenceError from "./app/errors/InvalidSequenceError";

const fs = require('fs');
const path = require('path');
const readline = require('readline');

const INPUT_FILES_DIR = './input';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/**
 *
 */
const infiniteInputOutput = () => {
    rl.question(`Enter filename (from directory \'${INPUT_FILES_DIR}\'): `, function (filename) {
        handleFile(filename);
        infiniteInputOutput();
    });
};

/**
 * @param {string} filename
 */
const handleFile = filename => {
    const absolutePath = path.resolve(`${INPUT_FILES_DIR}/${filename}`);
    console.log(`Absolute file path is: ${absolutePath}`);

    try {
        const fileContents = fs.readFileSync(absolutePath);
        const inputSequence = NumericSequence.makeFromString(fileContents.toString());
        console.log('Input sequence: ', inputSequence.toString());
        console.log('Longest sequence with 0 is: ', SequenceService.getLongestSequence(inputSequence, 0).toString())
        console.log(
            'New sequence with ascending order by sequences length and alternating 1 sequences and 0 sequences: ',
            SequenceService.generateOrderedSequence(inputSequence, [0, 1]).toString()
        )
    } catch (err) {
        switch (true) {
            case err.code === 'ENOENT':
                console.error('File not found!');
                break;
            case err instanceof InvalidSequenceError:
                console.error(err.message);
                break;
            default:
                throw err;
        }
    }
}

infiniteInputOutput();
