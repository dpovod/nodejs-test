# README #

### How to deploy this project ###

* `cp .env.example .env`
* `docker-compose up -d`

### How to run the app ###

* Put your input file in the '.input/' directory
* Run `docker exec -it <CONTAINER_NAME> node index.js` in terminal
* Enter your file name in the prompt

### How to run unit tests ###

`docker exec -it <CONTAINER_NAME> npm run test`
