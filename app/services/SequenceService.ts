import NumericSequence from "../valueObjects/NumericSequence";
import MonoNumericSequence from "../valueObjects/MonoNumericSequence";
import NumericSequenceList from "../valueObjects/NumericSequenceList";
import InvalidSequenceError from "../errors/InvalidSequenceError";

class SequenceService {
    /**
     * @param {NumericSequence} sequence
     * @param {number} numberToSearch
     * @return {NumericSequence}
     * @throws Error
     */
    static getLongestSequence(sequence: NumericSequence, numberToSearch: number) {
        const subsequencesList = SequenceService.explodeToMonoSequences(sequence, [numberToSearch]);

        return subsequencesList.getLongestSequence();
    }

    /**
     * @param {NumericSequence} sequence
     * @param {number[]} allowedMembers
     * @return {NumericSequence}
     * @throws Error
     */
    static generateOrderedSequence(sequence: NumericSequence, allowedMembers: number[]) {
        let subsequences = SequenceService.explodeToMonoSequences(sequence, [0, 1]).sortByLength();
        let monoSubsequencesLists: NumericSequenceList[] = [];

        allowedMembers.forEach(allowedMember => {
            monoSubsequencesLists[allowedMember] = subsequences.getOnlyMonoSequences(allowedMember);
        });

        let subsequencesCount: number | null = null;

        monoSubsequencesLists.forEach(subsequencesList => {
            if (subsequencesCount !== null && subsequencesCount !== subsequencesList.getCount()) {
                throw new InvalidSequenceError('Count of subsequences of different numbers is not match');
            }

            subsequencesCount = subsequencesList.getCount();
        });

        let needIterate: boolean = true, resultSequence: NumericSequence = new NumericSequence([]);

        while (needIterate) {
            for (let i = allowedMembers.length - 1; i >= 0; i--) {
                let currentSubsequence = monoSubsequencesLists[i].shift();

                if (currentSubsequence) {
                    resultSequence.addMembers(currentSubsequence.members);
                    continue;
                }

                needIterate = false;
            }
        }

        return resultSequence;
    }

    /**
     * @param {NumericSequence} sequence
     * @param {number[]} allowedMembers
     * @return {NumericSequenceList}
     */
    static explodeToMonoSequences(sequence: NumericSequence, allowedMembers: number[]) {
        let subsequences: Array<MonoNumericSequence | NumericSequence> = [];
        let currentSubSequenceLength: number = 1;

        sequence.members.forEach((number, i, array) => {
            if (allowedMembers.indexOf(number) === -1) {
                return;
            }

            const prevNumber = array[i - 1];
            const nextNumber = array[i + 1];
            const startSubsequence = prevNumber !== number;
            const endSubsequence = nextNumber !== number;
            const insideSubsequence = !startSubsequence && !endSubsequence;

            if (startSubsequence) {
                currentSubSequenceLength = 1;
            }

            const needIncrease = !startSubsequence && (insideSubsequence || endSubsequence)

            if (needIncrease) {
                currentSubSequenceLength++;
            }

            if (endSubsequence) {
                subsequences.push(new MonoNumericSequence(Array(currentSubSequenceLength).fill(number), number));
            }
        });

        return new NumericSequenceList(subsequences);
    }
}

export default SequenceService;
