class InvalidSequenceError extends Error {
    constructor(msg = 'Invalid sequence') {
        super(msg);
        Object.setPrototypeOf(this, InvalidSequenceError.prototype)
        this.name = 'InvalidSequenceError';
    }
}

export default InvalidSequenceError;
