import NumericSequence from "./NumericSequence";
import MonoNumericSequence from "./MonoNumericSequence";

class NumericSequenceList {
    sequences: NumericSequence[];

    /**
     * @param {NumericSequence[]} sequences
     */
    constructor(sequences: NumericSequence[]) {
        this.sequences = sequences;
    }

    /**
     * @return {NumericSequence}
     */
    getLongestSequence() {
        let longestSequence: NumericSequence = new NumericSequence([]);

        this.sequences.forEach(sequence => {
            if (longestSequence === null || longestSequence.getLength() < sequence.getLength()) {
                longestSequence = sequence;
            }
        });

        return longestSequence;
    }

    /**
     * @return {number}
     */
    getCount() {
        return this.sequences.length;
    }

    /**
     * @param {string} sortDir
     * @return {NumericSequenceList}
     */
    sortByLength(sortDir: string = 'asc') {
        switch (sortDir) {
            case 'asc':
                this.sequences.sort((a, b) => {
                    return a.getLength() - b.getLength();
                });
                return this;
            case 'desc':
                this.sequences.sort((a, b) => {
                    return b.getLength() - a.getLength();
                });
                return this;
            default:
                throw new Error(`Sort direction ${sortDir} not allowed.`);
        }
    }

    /**
     * @param {number} allowedMember
     * @return NumericSequenceList
     */
    getOnlyMonoSequences(allowedMember: number) {
        const newSequences = this.sequences.filter(sequence => {
            return sequence instanceof MonoNumericSequence && sequence.allowedMember === allowedMember;
        });

        return new NumericSequenceList(newSequences);
    }

    /**
     * @return {NumericSequence|undefined}
     */
    shift() {
        return this.sequences.shift();
    }
}

export default NumericSequenceList;
