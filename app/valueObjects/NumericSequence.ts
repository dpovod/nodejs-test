class NumericSequence {
    members: number[];

    /**
     * @param {number[]} members
     */
    constructor(members: number[]) {
        this.members = members;
    }

    /**
     * @return {number}
     */
    getLength() {
        return this.members.length;
    }

    /**
     * @param {string} sequence
     * @return NumericSequence
     */
    static makeFromString(sequence: string) {
        sequence = sequence.replace(/[\[\]\s\n\'\"]/g, '');
        let array = sequence.split(',');

        const members = array.map((value, i) => {
            const number = parseInt(value);

            if (isNaN(number)) {
                throw new Error(`Unable to parse integer at position ${i}`);
            }

            return number;
        });

        return new NumericSequence(members);
    }

    /**
     * @param {number} member
     * @param {number} length
     * @return NumericSequence
     */
    static fill(member: number, length: number) {
        const members = Array(length).fill(member);

        return new NumericSequence(members);
    }

    /**
     * @param {number[]} members
     * @return self
     */
    addMembers(members: number[]) {
        this.members = this.members.concat(members);

        return this;
    }

    /**
     * @return {string}
     */
    toString() {
        return this.members.join(',');
    }
}

export default NumericSequence;
