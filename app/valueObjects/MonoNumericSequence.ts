import NumericSequence from "./NumericSequence";

class MonoNumericSequence extends NumericSequence {
    allowedMember: number;

    /**
     * @param {number[]} members
     * @param {number} allowedMember
     */
    constructor(members: number[], allowedMember: number) {
        members.forEach(member => {
            if (member !== allowedMember) {
                throw new Error(`Member ${member} is not allowed for this sequence.`);
            }
        });

        super(members);
        this.allowedMember = allowedMember;
    }

}

export default MonoNumericSequence;
